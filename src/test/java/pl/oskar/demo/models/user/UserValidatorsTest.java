package pl.oskar.demo.models.user;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import pl.oskar.demo.validators.UserValidators;

class UserValidatorsTest {

    UserValidators uv;

    @BeforeEach
    void setUp() {

        uv = new UserValidators();
    }

    @Test
    void shouldReturnFalseIfPasswordsAreNotTheSame(){
        Assertions.assertThat(uv.repeatedPasswordIsCorrect("password123","dog")).isFalse();
    }

    @ParameterizedTest
    @CsvSource({
            "password123",
            "Password123",
            "@Pass1"
    })
    void shouldReturnFalseIfPasswordIsNotStrong(String password){
        Assertions.assertThat(uv.passwordIsStrong(password)).isFalse();
    }

    @Test
    void shouldReturnTrueIfPasswordIsStrong(){
        Assertions.assertThat(uv.passwordIsStrong("@Password123!")).isTrue();
    }

}