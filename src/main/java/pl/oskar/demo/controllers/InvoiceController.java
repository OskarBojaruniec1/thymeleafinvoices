package pl.oskar.demo.controllers;

import com.sun.tools.javac.Main;
import lombok.Getter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.oskar.demo.models.Invoice;
import pl.oskar.demo.models.User;

import java.util.ArrayList;
import java.util.List;

@Controller
public class InvoiceController {


    @Getter
    private static List<Invoice> invoices;

    public InvoiceController() {
        invoices = new ArrayList<>();
    }

    @PostMapping("/create-invoice")
    public String createInvoice(@ModelAttribute Invoice invoice, Model model) {

        model.addAttribute("newInvoice", new Invoice());
        invoices.add(invoice);

        return "redirect:/table-of-invoices";
    }
}
