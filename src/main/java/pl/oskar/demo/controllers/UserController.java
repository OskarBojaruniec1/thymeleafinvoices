package pl.oskar.demo.controllers;


import lombok.Getter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.oskar.demo.models.User;
import pl.oskar.demo.validators.UserValidators;

import java.util.ArrayList;
import java.util.List;


@Controller

public class UserController {

    @Getter
    private static List<User> users;

    public UserController() {
        users = new ArrayList<>();
    }

    @PostMapping("/create-account")
    public String createAccount(@ModelAttribute User user,Model model) {

        UserValidators uv = new UserValidators();

        model.addAttribute("newUser", new User());

        if(!uv.repeatedPasswordIsCorrect(user.getPassword(), user.getRepeatedPassword())){
            model.addAttribute("errorMessage","Passwords are not matches");
            return "registration";
        }

        if(!uv.passwordIsStrong(user.getPassword())){
            model.addAttribute("errorMessage","Password is too weak");
            return "registration";
        }

        if(uv.loginIsUsed(user.getLogin())){
            model.addAttribute("errorMessage","Login is already used");
            return "registration";
        }

        users.add(user);

        return "redirect:/";
    }

    @PostMapping("/login-into-acc")
    public String login(@ModelAttribute User user,Model model){

        UserValidators uv = new UserValidators();

        model.addAttribute("newUser", new User());

        if(uv.isUserExist(user.getLogin(), user.getPassword()))  return "redirect:/table-of-invoices";

        model.addAttribute("errorMessage", "Login or password is incorrect");
        return "login";

    }
}
