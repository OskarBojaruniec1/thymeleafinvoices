package pl.oskar.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.oskar.demo.models.Invoice;
import pl.oskar.demo.models.User;

import java.util.List;

@Controller
public class MainMenuController {

    InvoiceController ic = new InvoiceController();


    @GetMapping("/")
    public String mainMenu(){

        return "index";
    }
    @GetMapping("/login")
    public String loginMenu(Model model){

        model.addAttribute("newUser",new User());
        return "login";
    }

    @GetMapping("/registration")
    public String registrationMenu(Model model){

        model.addAttribute("newUser",new User());

        return "registration";
    }

    @GetMapping("/table-of-invoices")
    public String tableOfInvoicesMenu(Model model){

        List<Invoice> invoicesList = InvoiceController.getInvoices();

        model.addAttribute("invoicesList", invoicesList);
        return "tableOfInvoices";
    }

    @GetMapping("/create-invoice-menu")
    public String createInvoiceMenu(Model model){

        model.addAttribute("newInvoice",new Invoice());
        return "createInvoice";
    }

    @GetMapping("/delete/{contractor}")
    public String delete(@PathVariable String contractor) {

        InvoiceController.getInvoices().removeIf(v -> v.getContractor().equals(contractor));

        return "redirect:/table-of-invoices";
    }




}
