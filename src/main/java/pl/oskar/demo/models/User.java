package pl.oskar.demo.models;

import lombok.*;

@Getter
@Setter
@ToString
public class User {

    private String login;
    private String password;
    private String repeatedPassword;
    private String email;
}

