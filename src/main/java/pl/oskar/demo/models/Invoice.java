package pl.oskar.demo.models;


import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {

    private String date;
    private int invoiceNumber;
    private String contractor;
    private String description;
    private double amount;
    private int paymentTime;
}
