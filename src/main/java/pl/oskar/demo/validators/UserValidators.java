package pl.oskar.demo.validators;

import pl.oskar.demo.controllers.UserController;
import pl.oskar.demo.models.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class UserValidators {

    public boolean repeatedPasswordIsCorrect(String password, String repeatedPassword) {
        return password.equals(repeatedPassword);
    }

    public boolean passwordIsStrong(String password) {

        Pattern compiledPattern = Pattern.compile("^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*\\W).{8,32}$");
        Matcher matcher = compiledPattern.matcher(password);

        return matcher.matches();
    }
    public boolean loginIsUsed(String login){


        for (User x: UserController.getUsers()) {

            if(login.equals(x.getLogin())) return true;
        }

        return false;
    }

    public boolean isUserExist(String login,String password){

        for (User x: UserController.getUsers()) {

            if(login.equals(x.getLogin()) && password.equals(x.getPassword())) return true;
        }

        return false;
    }
}
