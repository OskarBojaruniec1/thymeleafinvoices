package pl.oskar.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvoiceDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvoiceDbApplication.class, args);
	}

}
